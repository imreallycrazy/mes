package com.lgl.mes.barcode.mapper;

import com.lgl.mes.barcode.entity.SpBarcode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2023-12-13
 */
public interface SpBarcodeMapper extends BaseMapper<SpBarcode> {

    //@Update({ "update sp_barcode set queryTimes = #{queryTimes} ,first_time = #{first_time, jdbcType=TIMESTAMP} where id = #{id}" })

    @Update({ "update sp_barcode set query_times = #{queryTimes} where id = #{id}" })

    //  @Update({ "update SpProduct set productId = #{productId}   where id = #{id}" })
    //@Update({ "update SpBarcode set queryTimes = #{queryTimes} where id = #{id}" })
    int updateById_lgl1111 ( SpBarcode  bc  );

    @Update({ "update sp_barcode set query_times = #{queryTimes}, first_time = #{firstTime}    where id = #{id}" })
    int updateById_lgl ( SpBarcode  bc  );

}
