package com.lgl.mes.log.service.impl;

import com.lgl.mes.log.entity.SpSysLog;
import com.lgl.mes.log.mapper.SpSysLogMapper;
import com.lgl.mes.log.service.ISpSysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志  服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-09
 */
@Service
public class SpSysLogServiceImpl extends ServiceImpl<SpSysLogMapper, SpSysLog> implements ISpSysLogService {

}
