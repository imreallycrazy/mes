package com.lgl.mes.common.util.org.easy.excel.parsing;


import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import com.lgl.mes.common.util.org.easy.excel.ExcelDefinitionReader;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptor;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.config.ExcelDefinition;
import com.lgl.mes.common.util.org.easy.excel.config.FieldValue;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelDataException;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelImportLimitException;
import com.lgl.mes.common.util.org.easy.excel.result.ExcelImportResult;
import com.lgl.mes.common.util.org.easy.excel.util.ExcelUtil;
import org.springframework.beans.AbstractPropertyAccessor;
import org.springframework.beans.BeanUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * Excel导入实现类
 * @author lisuo
 *
 */
public class ExcelImport extends AbstractExcelResolver{
	
	
	public ExcelImport(ExcelDefinitionReader definitionReader) {
		super(definitionReader);
	}
	
	/**
	 * 读取Excel信息
	 * @param id 注册的ID
	 * @param titleIndex 标题索引
	 * @param excelStream Excel文件流
	 * @param sheetIndex Sheet索引位置
	 * @param multivalidate 是否逐条校验，默认单行出错立即抛出ExcelException，为true时为批量校验,可通过ExcelImportResult.hasErrors,和getErrors获取具体错误信息
	 * @return
	 * @throws Exception
	 */
	public ExcelImportResult readExcel(String id, int titleIndex,InputStream excelStream,Integer sheetIndex,boolean multivalidate) {
		//从注册信息中获取Bean信息
		ExcelDefinition excelDefinition = definitionReader.getRegistry().get(id);
		if(excelDefinition==null){
			throw new ExcelException("没有找到 ["+id+"] 的配置信息");
		}
		return doReadExcel(excelDefinition,titleIndex,excelStream,sheetIndex,multivalidate);
	}
	
	protected ExcelImportResult doReadExcel(ExcelDefinition excelDefinition,int titleIndex,InputStream excelStream,Integer sheetIndex,boolean multivalidate) {
		Workbook workbook = ExcelUtil.getWorkBookByStream(excelStream);
		try {
			ExcelImportResult result = new ExcelImportResult();
			//读取sheet,sheetIndex参数优先级大于ExcelDefinition配置sheetIndex
			Sheet sheet = ExcelUtil.getSheetAt(workbook, sheetIndex==null?excelDefinition.getSheetIndex():sheetIndex);
			//标题之前的数据处理
			List<List<Object>> header = readHeader(excelDefinition, sheet,titleIndex);
			result.setHeader(header);
			//获取标题
			List<String> titles = readTitle(excelDefinition,sheet,titleIndex);
			//校验标题
			checkTitle(excelDefinition, titles);
			//获取Bean
			List<Object> listBean = readRows(result,excelDefinition,titles, sheet,titleIndex,multivalidate);
			result.setListBean(listBean);
			return result;
		} finally{
			ExcelUtil.closeBook(workbook);
		}
		
	}
	
	/**
	 * 解析标题之前的内容,如果ExcelDefinition中titleIndex 不是0
	 * @param excelDefinition
	 * @param sheet
	 * @return
	 */
	protected List<List<Object>> readHeader(ExcelDefinition excelDefinition,Sheet sheet,int titleIndex){
		List<List<Object>> header = null;
		if(titleIndex!=0){
			header = new ArrayList<List<Object>>(titleIndex);
			for(int i=0;i<titleIndex;i++){
				Row row = sheet.getRow(i);
				if(row == null) {
					continue;
				}
				short cellNum = row.getLastCellNum();
				List<Object> item = new ArrayList<Object>(cellNum);
				for(int j=0;j<cellNum;j++){
					Cell cell = row.getCell(j);
					Object value = getCellValue(cell);
					item.add(value);
				}
				header.add(item);
			}
		}
		return header;
	}
	
	/**
	 * 读取多行
	 * @param result
	 * @param excelDefinition
	 * @param titles
	 * @param sheet
	 * @param titleIndex
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected <T> List<T> readRows(ExcelImportResult result,ExcelDefinition excelDefinition, List<String> titles, Sheet sheet,int titleIndex,boolean multivalidate) {
		//读取数据的总共次数
		int totalNum = sheet.getLastRowNum() - titleIndex;
		if(totalNum > excelDefinition.getLimit()) {
			throw new ExcelImportLimitException("导入的数据条数不能超过["+excelDefinition.getLimit()+"]条",excelDefinition.getLimit());
		}
		result.setTotalNum(totalNum);
		result.setValidListBean(new ArrayList<T>(totalNum));
		List<T> listBean = new ArrayList<T>(totalNum);
		for (int rowNum = 1; rowNum <= totalNum; rowNum++) {
			try {
				//处理索引位置,为标题索引位置+数据行
				Row row = sheet.getRow(rowNum + titleIndex);
				Object bean = readRow(excelDefinition,row,titles,rowNum,multivalidate,result);
				if(bean!=null) {
					listBean.add((T) bean);
				}
			}catch(ExcelException e) {
				throw e;
			}
		}
		return listBean;
	}
	
	/**
	 * 读取1行
	 * @param excelDefinition
	 * @param row
	 * @param titles
	 * @param rowNum 第几行
	 * @param multivalidate
	 * @param result
	 * @return
	 */
	protected Object readRow(ExcelDefinition excelDefinition, Row row, List<String> titles,int rowNum,boolean multivalidate,ExcelImportResult result) {
		//判断当前行是否存在数据
		if(row == null || row.getLastCellNum() <=0) {
			return null;
		}
		//创建注册时配置的bean类型
		Object bean = BeanUtils.instantiateClass(excelDefinition.getClazz());
		boolean hasError = false;
		AbstractPropertyAccessor accessor = BeanUtil.buildAccessor(bean, true);
		for(FieldValue fieldValue:excelDefinition.getFieldValues()){
			for (int j = 0; j < titles.size(); j++) {
				//标题或者别名eq
				if(fieldValue.getTitle().equals(titles.get(j)) || fieldValue.getAlias().equals(titles.get(j))){
					try {
						Cell cell = row.getCell(j);
						//获取Excel原生value值
						Object originalValue = getCellValue(cell);
						Object value = originalValue;
						for (CellInterceptor interceptor : getCellInterceptors()) {
							CellJoinPoint joinPoint = new CellJoinPoint(excelDefinition, fieldValue, accessor, bean, rowNum, value, originalValue,ctx);
							if(interceptor.supports(joinPoint)) {
								value = interceptor.executeImport(joinPoint);
							}
						}
						BeanUtil.setPropertyValue(accessor, fieldValue.getName(), value, false);
						break;
					}catch(ExcelDataException e) {
						hasError = true;
						if(multivalidate) {
							result.getErrors().add(e);
						}else {
							throw e;
						}
					}
				}
			}
		}
		if(hasError) {
			result.getErrorListBean().add(bean);
		}else {
			result.getValidListBean().add(bean);
		}
		return bean;
	}

	protected List<String> readTitle(ExcelDefinition excelDefinition, Sheet sheet,int titleIndex) {
		// 获取Excel标题数据
		Row hssfRowTitle = sheet.getRow(titleIndex);
		if(hssfRowTitle==null){
			return null;
		}
		int cellNum = hssfRowTitle.getLastCellNum();
		List<String> titles = new ArrayList<String>(cellNum);
		// 获取标题数据
		for (int i = 0; i < cellNum; i++) {
			Cell cell = hssfRowTitle.getCell(i);
			if(cell == null) {
				continue;
			}
			Object value = getCellValue(cell);
			if(value == null || "".equals(value.toString().trim())) {
				return null;
			}
			titles.add(value.toString());
		}
		return titles;
	}
	
	/**
	 * 校验excel标题与配置的标题是否匹配,如果不包含抛出异常
	 * @param excelDefinition
	 * @param titles
	 */
	private void checkTitle(ExcelDefinition excelDefinition,List<String> titles){
		if(CollectionUtils.isEmpty(titles)){
			throw new ExcelException("标题不能为空");
		}
		List<FieldValue> fieldValues = excelDefinition.getFieldValues();
		//标题校验规则：excel中没有对应的配置标题或别名，同时该属性不能为空，如果为空，允许标题不存在
		for (FieldValue fieldValue : fieldValues) {
			if(!titles.contains(fieldValue.getTitle())){
				if(!titles.contains(fieldValue.getAlias())){
					if(!fieldValue.isNull()){
						throw new ExcelException("标题["+fieldValue.getAlias()+"]在Excel中不存在");
					}
				}
			}
		}
	}
	
}
