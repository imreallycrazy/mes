package com.lgl.mes.group.service;

import com.lgl.mes.group.entity.SpGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工序表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
public interface ISpGroupService extends IService<SpGroup> {

}
