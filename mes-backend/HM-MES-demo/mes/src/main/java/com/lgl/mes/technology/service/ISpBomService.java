package com.lgl.mes.technology.service;

import com.lgl.mes.technology.entity.SpBom;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  bom服务类
 * </p>
 *
 * @author lgl
 * @since 2020-03-28
 */
public interface ISpBomService extends IService<SpBom> {

    public String LocalImport(MultipartFile file);

    public List<String>   GetBomList();
}
