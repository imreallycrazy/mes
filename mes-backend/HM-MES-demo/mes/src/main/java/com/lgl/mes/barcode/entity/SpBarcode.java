package com.lgl.mes.barcode.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 *
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2023-12-13
 */
@ApiModel(value="SpBarcode对象", description="")
@TableName("sp_barcode")
public class SpBarcode extends BaseEntity   {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "基本url")
    private String baseUrl;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "参数")
    private String param;

    @ApiModelProperty(value = "图片名称")
    private String imgName;

    @ApiModelProperty(value = "图片完整路径")
    private String fullImgUrl;

    @ApiModelProperty(value = "查询次数")
    private Integer queryTimes;

    @ApiModelProperty(value = "初次查询时间")
    private LocalDateTime firstTime;

    @ApiModelProperty(value = "逻辑删除：1 表示删除，0 表示未删除，2 表示禁用")
    private Integer isDeleted;

    @ApiModelProperty(value = "备注")
    private String memo;

    @ApiModelProperty(value = "1 表示已经打印，0 表示未未打印")
    private Integer isPrinted;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }
    public String getFullImgUrl() {
        return fullImgUrl;
    }

    public void setFullImgUrl(String fullImgUrl) {
        this.fullImgUrl = fullImgUrl;
    }


    public Integer getQueryTimes() {
        return queryTimes;
    }

    public void setQueryTimes(Integer queryTimes) {
        this.queryTimes = queryTimes;
    }


    public LocalDateTime getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(LocalDateTime firstTime) {
        this.firstTime = firstTime;
    }


    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }


    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }


    public Integer getIsPrinted() {
        return isPrinted;
    }

    public void setIsPrinted(Integer isPrinted) {
        this.isPrinted = isPrinted;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpBarcode{" +
            "baseUrl=" + baseUrl +
            ", code=" + code +
            ", param=" + param +
            ", imgName=" + imgName +
            ", fullImgUrl=" + fullImgUrl +
            ", queryTimes=" + queryTimes +
            ", firstTime=" + firstTime +
            ", isDeleted=" + isDeleted +
            ", memo=" + memo +
            ", isPrinted=" + isPrinted +
        "}";
    }
}
