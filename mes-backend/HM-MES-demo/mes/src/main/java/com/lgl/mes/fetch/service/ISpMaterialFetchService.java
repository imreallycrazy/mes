package com.lgl.mes.fetch.service;

import com.lgl.mes.fetch.entity.SpMaterialFetch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 基础物料表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-12-10
 */
public interface ISpMaterialFetchService extends IService<SpMaterialFetch> {

}
