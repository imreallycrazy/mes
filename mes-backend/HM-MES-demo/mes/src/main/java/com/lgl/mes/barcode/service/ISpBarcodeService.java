package com.lgl.mes.barcode.service;

import com.lgl.mes.barcode.entity.SpBarcode;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lgl.mes.barcode.request.spBarcodeBatchAddReq;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2023-12-13
 */
public interface ISpBarcodeService extends IService<SpBarcode> {




    public  int updateById_lgl ( SpBarcode  bc  );
    boolean addBatch  (/*HttpServletRequest request,*/spBarcodeBatchAddReq req) throws UnknownHostException;
}
