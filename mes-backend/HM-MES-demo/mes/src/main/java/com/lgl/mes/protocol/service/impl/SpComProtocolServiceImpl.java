package com.lgl.mes.protocol.service.impl;

import com.lgl.mes.protocol.entity.SpComProtocol;
import com.lgl.mes.protocol.mapper.SpComProtocolMapper;
import com.lgl.mes.protocol.service.ISpComProtocolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线体表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@Service
public class SpComProtocolServiceImpl extends ServiceImpl<SpComProtocolMapper, SpComProtocol> implements ISpComProtocolService {

}
