package com.lgl.mes.rabbitmq.spring.controller;


/*
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lgl.mes.setting.entity.SpGlobalSetting;
import com.lgl.mes.setting.service.ISpGlobalSettingService;
import com.lgl.mes.trace.entity.SpMaterialTrace;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.lgl.mes.rabbitmq.spring.po.Mail;
import com.lgl.mes.rabbitmq.spring.po.TopicMail;
import com.lgl.mes.rabbitmq.spring.service.impl.ProducerImpl;
import com.lgl.mes.rabbitmq.spring.service.impl.PublisherImpl;
import com.lgl.mes.rabbitmq.spring.service.Producer;
import com.lgl.mes.rabbitmq.spring.service.Publisher;
import  com.lgl.mes.setting.service.ISpGlobalSettingService;

import java.util.List;

@Controller
@RequestMapping("/rabbitmq")
public class RabbitMQController {
	@Autowired
	Producer producer;
	
	@Autowired
	Publisher publisher;

	@Autowired
	ISpGlobalSettingService iSpGlobalSettingService;


	@RequestMapping(value="/produce",produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public void produce(@ModelAttribute("mail")Mail mail) throws Exception{
		producer.sendMail("myqueue",mail);
	}
	
	@RequestMapping(value="/topic",produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public void topic(@ModelAttribute("mail")Mail mail) throws Exception{
		publisher.publishMail(mail);
	}
	
	@RequestMapping(value="/direct",produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public void direct(@ModelAttribute("mail")TopicMail mail){
		Mail m=new Mail("",mail.getOrderNo(),mail.getProductId(), mail.getQuality(), (double) 0);
		publisher.senddirectMail(m, mail.getRoutingkey());
	}
	
	@RequestMapping(value="/mytopic",produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public void topic(@ModelAttribute("mail")TopicMail mail){
		Mail m=new Mail("",mail.getOrderNo(),mail.getProductId(), mail.getQuality(), (double) 0);
		publisher.sendtopicMail(m, mail.getRoutingkey());
	}
	

	@GetMapping("/demo")
	public String demo(){
		return "rabbitmq/demo";
	}

	@ApiOperation("rabbit admin")
	@GetMapping("/admin")
	public String admin() {
		QueryWrapper<SpGlobalSetting> queryWrapper =new QueryWrapper();
		queryWrapper.eq("line","磁阻变压器");
		List<SpGlobalSetting>  list  = iSpGlobalSettingService.list(queryWrapper) ;
		SpGlobalSetting gs = list.get(0) ;
		String url = "redirect:" + gs.getRabbitmqurl();
		System.out.println("rabbit url = "+ url);
		return  url;
	}




 */


		/*if(false)
			return "redirect:http://47.240.54.105:15672/#/";
		else
			return "redirect:http://127.0.0.1:15672/#/";
		*/


//}


