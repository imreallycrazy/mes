package com.lgl.mes.group.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 工序表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@ApiModel(value="SpGroup对象", description="工序表")
public class SpGroup extends BaseEntity{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "班组名称")
    private String name;

    @ApiModelProperty(value = "班长")
    private String leader;

    @ApiModelProperty(value = "班组成员数量")
    private Integer groupCount;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "逻辑删除：1 表示删除，0 表示未删除，2 表示禁用")
    private String isDeleted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }
    public Integer getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpGroup{" +
            "name=" + name +
            ", leader=" + leader +
            ", groupCount=" + groupCount +
            ", contact=" + contact +
            ", isDeleted=" + isDeleted +
        "}";
    }
}
