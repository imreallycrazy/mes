package com.lgl.mes.protocol.mapper;

import com.lgl.mes.protocol.entity.SpComProtocol;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线体表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
public interface SpComProtocolMapper extends BaseMapper<SpComProtocol> {

}
