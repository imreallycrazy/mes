package com.lgl.mes.common.util.org.easy.excel.parsing;


import org.apache.poi.ss.usermodel.Cell;
import com.lgl.mes.common.util.org.easy.excel.ExcelDefinitionReader;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptor;
import com.lgl.mes.common.util.org.easy.excel.config.GlobalConfig;
import com.lgl.mes.common.util.org.easy.excel.util.ExcelUtil;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * Excel抽象解析器
 * 
 * @author lisuo
 *
 */
public abstract class AbstractExcelResolver{

	protected ExcelDefinitionReader definitionReader;
	
	protected ApplicationContext ctx;

	public AbstractExcelResolver(ExcelDefinitionReader definitionReader) {
		this.definitionReader = definitionReader;
	}

	/**
	 * 设置Cell单元的值
	 * 
	 * @param cell
	 * @param value
	 */
	protected void setCellValue(Cell cell, Object value) {
		ExcelUtil.setCellValue(cell, value);
	}

	/**
	 * 获取cell值
	 * 
	 * @param cell
	 * @return
	 */
	protected Object getCellValue(Cell cell) {
		return ExcelUtil.getCellValue(cell);
	}
	
	public List<CellInterceptor> getCellInterceptors() {
		return GlobalConfig.getInstance().getCellInterceptors();
	}
	
	
	/**
	 * 注入spring context
	 * @param applicationContext
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.ctx = applicationContext;
	}
	
}
