package com.lgl.mes.group.service.impl;

import com.lgl.mes.group.entity.SpGroup;
import com.lgl.mes.group.mapper.SpGroupMapper;
import com.lgl.mes.group.service.ISpGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工序表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
@Service
public class SpGroupServiceImpl extends ServiceImpl<SpGroupMapper, SpGroup> implements ISpGroupService {

}
