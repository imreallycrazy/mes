package com.lgl.mes.common.util;
import javax.servlet.http.HttpServletRequest;

public class ServerAddressExample {

    public static String getServerAddress(HttpServletRequest request) {
        // 获取协议（http 或 https）
        String protocol = request.getScheme();

        // 获取服务器名称
        String serverName = request.getServerName();

        // 获取服务器端口号
        int serverPort = request.getServerPort();

        // 构建完整的服务器地址
        String serverAddress = protocol + "://" + serverName + ":" + serverPort;

        return serverAddress;
    }

}