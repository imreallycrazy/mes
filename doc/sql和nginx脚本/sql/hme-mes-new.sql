/*
 Navicat Premium Dump SQL

 Source Server         : 150.158.15.149-3307-lgl..ex..
 Source Server Type    : MySQL
 Source Server Version : 80030 (8.0.30)
 Source Host           : 150.158.15.149:3307
 Source Schema         : hme-mes-new

 Target Server Type    : MySQL
 Target Server Version : 80030 (8.0.30)
 File Encoding         : 65001

 Date: 12/08/2024 03:11:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sp_barcode
-- ----------------------------
DROP TABLE IF EXISTS `sp_barcode`;
CREATE TABLE `sp_barcode`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `base_url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '基本url',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '127.0.0.1' COMMENT '编码',
  `param` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '9600' COMMENT '参数',
  `img_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '9600' COMMENT '图片名称',
  `full_img_url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '10' COMMENT '图片完整路径',
  `query_times` int NULL DEFAULT 0 COMMENT '查询次数',
  `first_time` datetime NULL DEFAULT NULL COMMENT '初次查询时间',
  `is_deleted` tinyint NOT NULL DEFAULT 0 COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `memo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '10' COMMENT '备注',
  `is_printed` tinyint NOT NULL DEFAULT 0 COMMENT '1 表示已经打印，0 表示未未打印',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_barcode
-- ----------------------------
INSERT INTO `sp_barcode` VALUES ('1741978668846219266', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', '3KPJ55IXDLOVLI', '?papa=0', '3KPJ55IXDLOVLI.png', '172.17.39.203:8088/images/3KPJ55IXDLOVLI.png', 0, NULL, 0, 'www.dreammm.net3KPJ55IXDLOVLI&?papa=0', 0);
INSERT INTO `sp_barcode` VALUES ('1741978669655719938', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', 'PH7HYXP35KP9PI', '?papa=0', 'PH7HYXP35KP9PI.png', '172.17.39.203:8088/images/PH7HYXP35KP9PI.png', 0, NULL, 0, 'www.dreammm.netPH7HYXP35KP9PI&?papa=0', 0);
INSERT INTO `sp_barcode` VALUES ('1741978670133870593', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', 'FA555WVDJYIH48', '?papa=0', 'FA555WVDJYIH48.png', '172.17.39.203:8088/images/FA555WVDJYIH48.png', 0, NULL, 0, 'www.dreammm.netFA555WVDJYIH48&?papa=0', 0);
INSERT INTO `sp_barcode` VALUES ('1741978670653964290', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', '415PQC3KP07TUH', '?papa=0', '415PQC3KP07TUH.png', '172.17.39.203:8088/images/415PQC3KP07TUH.png', 0, NULL, 0, 'www.dreammm.net415PQC3KP07TUH&?papa=0', 0);
INSERT INTO `sp_barcode` VALUES ('1741978671153086465', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', 'N8098XDX9CLFCZ', '?papa=0', 'N8098XDX9CLFCZ.png', '172.17.39.203:8088/images/N8098XDX9CLFCZ.png', 0, NULL, 0, 'www.dreammm.netN8098XDX9CLFCZ&?papa=0', 0);
INSERT INTO `sp_barcode` VALUES ('1741978671627042817', '2024-01-02 08:24:14', 'admin', '2024-01-02 08:24:14', 'admin', 'www.dreammm.net', 'MMAU2QRWGEQVX7', '?papa=0', 'MMAU2QRWGEQVX7.png', '172.17.39.203:8088/images/MMAU2QRWGEQVX7.png', 0, NULL, 0, 'www.dreammm.netMMAU2QRWGEQVX7&?papa=0', 0);

-- ----------------------------
-- Table structure for sp_bom
-- ----------------------------
DROP TABLE IF EXISTS `sp_bom`;
CREATE TABLE `sp_bom`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `bom_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'bom编号',
  `materiel_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料ID',
  `materiel_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料描述',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `version_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '版本号',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'BOM状态 creat创建 pass审核通过 ',
  `factory` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工厂',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'BOM主信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_bom
-- ----------------------------
INSERT INTO `sp_bom` VALUES ('1822064831546499073', '磁阻传感器', 'THREAD001001', '铜线', '', '1', NULL, '富士康', '0', '2024-08-10 08:18:03', 'admin', '2024-08-10 08:18:03', 'admin');
INSERT INTO `sp_bom` VALUES ('1822065073134215169', '磁阻传感器', 'PCB1010000388', 'pcb', '', '1', NULL, '南方电路', '0', '2024-08-10 08:19:00', 'admin', '2024-08-10 08:19:00', 'admin');
INSERT INTO `sp_bom` VALUES ('1822065198099308545', '磁阻传感器', 'DIPIAN', '底板', '', '1', NULL, '富士康', '0', '2024-08-10 08:19:30', 'admin', '2024-08-10 08:19:30', 'admin');
INSERT INTO `sp_bom` VALUES ('1822066893789306882', '测速传感器', 'THREAD001001', '铜线', '', '1', NULL, '富士康', '0', '2024-08-10 08:26:15', 'admin', '2024-08-10 08:26:15', 'admin');
INSERT INTO `sp_bom` VALUES ('1822067036563415042', '测速传感器', 'DIPIAN', '底片', '', '1', NULL, '富士康', '0', '2024-08-10 08:26:49', 'admin', '2024-08-10 08:26:49', 'admin');
INSERT INTO `sp_bom` VALUES ('1822067194751590402', '压力传感器', 'THREAD001001', '铜线', '', '1', NULL, '富士康', '0', '2024-08-10 08:27:26', 'admin', '2024-08-10 08:27:26', 'admin');
INSERT INTO `sp_bom` VALUES ('1822067333822128129', '压力传感器', 'DIPIAN', '底片', '', '1', NULL, '富士康', '0', '2024-08-10 08:27:59', 'admin', '2024-08-10 08:27:59', 'admin');

-- ----------------------------
-- Table structure for sp_bom_item
-- ----------------------------
DROP TABLE IF EXISTS `sp_bom_item`;
CREATE TABLE `sp_bom_item`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `bom_head_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'bom编号',
  `materiel_item_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物料ID',
  `materiel_item_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物料描述',
  `line_no` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '行号',
  `item_num` decimal(10, 0) NULL DEFAULT 0 COMMENT '用量',
  `item_unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子项基本单位',
  `oper_typer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属工序类型',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'BOM子项表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_bom_item
-- ----------------------------

-- ----------------------------
-- Table structure for sp_com_protocol
-- ----------------------------
DROP TABLE IF EXISTS `sp_com_protocol`;
CREATE TABLE `sp_com_protocol`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `protocol` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '协议名称',
  `protocol_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '协议描述，desc是sl保留关键字',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '线体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_com_protocol
-- ----------------------------
INSERT INTO `sp_com_protocol` VALUES ('18665802636', 'tcp', 'tcp', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802637', 'udp', 'udp', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802638', 'mqtt', 'mqtt', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802639', 'modbus', 'modbus', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802640', 'fins-tcp', 'fins-tcp', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802641', 'fins-udp', 'fins-udp', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802642', 'plc-simens', 'plc-simens', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802643', 'coap', 'coap', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802644', 'nb-iot', 'nb-iot', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');
INSERT INTO `sp_com_protocol` VALUES ('18665802645', 'http', 'http', '2022-08-30 10:55:11', 'admin', '2022-08-30 10:55:27', 'admin');

-- ----------------------------
-- Table structure for sp_config
-- ----------------------------
DROP TABLE IF EXISTS `sp_config`;
CREATE TABLE `sp_config`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `line_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生产线id',
  `server_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '127.0.0.1' COMMENT '服务器ip',
  `server_port` int NULL DEFAULT 9600 COMMENT '服务器port',
  `local_port` int NULL DEFAULT 9600 COMMENT '本地port',
  `interv` int NULL DEFAULT 10 COMMENT '更新数据的时间间隔 秒,interval是保留字不能使用',
  `com_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '4' COMMENT '通讯协议类型\r\n0 tcp   client\r\n1 tcp  server\r\n2 udp  server\r\n3 udp  client\r\n4 fins  client\r\n5 simens client\r\n',
  `use_comm` tinyint NULL DEFAULT 0 COMMENT '是否使用串口0 不适用， 1 使用',
  `bandrate` int NULL DEFAULT 115200 COMMENT '波特率',
  `data_bits` int UNSIGNED NULL DEFAULT 8 COMMENT '数据位',
  `stop_bits` int NULL DEFAULT 1 COMMENT '停止位',
  `check_bit` int NULL DEFAULT 0 COMMENT '0 奇校验，1偶校验',
  `fronturl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'front  url',
  `rabbitmqurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'rabbitmq url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_config
-- ----------------------------
INSERT INTO `sp_config` VALUES ('1555232008796508162', '2022-08-05 00:39:40', 'admin', '2024-08-09 15:21:45', 'admin', '磁阻传感器', '127.0.0.1', 9600, 9601, 10, 'tcp', 0, 115200, 8, 1, 0, 'http://127.0.0.1:8081', 'http://47.240.54.105:15672/#');
INSERT INTO `sp_config` VALUES ('1555232008796508163', '2022-08-05 00:39:40', 'admin', '2023-11-28 23:22:56', 'admin', '测速传感器', '47.240.54.105', 9600, 9602, 10, 'modbus', 0, 115200, 8, 1, 0, 'http://47.240.54.105:8081', '150.158.15.149');
INSERT INTO `sp_config` VALUES ('1601487510249377793', '2022-12-10 16:02:31', 'admin', '2024-07-09 14:27:43', 'admin', 'AT', '127.0.0.1', 50, 34, 10, 'plc-simens', 0, 115200, 8, 1, 0, '', '');
INSERT INTO `sp_config` VALUES ('1729521181614399490', '2023-11-28 23:22:37', 'admin', '2023-11-28 23:23:02', 'admin', 'dc001', '47.240.54.105', 9600, 9600, 10, 'http', 0, 115200, 8, 1, 0, '', '');

-- ----------------------------
-- Table structure for sp_daily_plan
-- ----------------------------
DROP TABLE IF EXISTS `sp_daily_plan`;
CREATE TABLE `sp_daily_plan`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT 'id',
  `plan_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '计划编码',
  `order_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编码',
  `plan_date` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '计划日期',
  `piece_time` int NULL DEFAULT 30 COMMENT '每件产品耗时 ，以秒为单位 (这个是设定的标准)',
  `real_piece_time` int NULL DEFAULT 30 COMMENT '实际每件耗时(平均每秒耗时,不是最近一件产品的耗时，也不是最近一段时间内的制造一件产品的耗时)',
  `plan_qty` int NULL DEFAULT 0 COMMENT '计划产量',
  `maked_qty` int NULL DEFAULT 0 COMMENT '当前产量',
  `bad_qty` int(10) UNSIGNED ZEROFILL NULL DEFAULT 0000000000 COMMENT '次品数量',
  `finish_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '当日完成率 ',
  `pass_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '一次通过率',
  `hour_qty` int NULL DEFAULT NULL COMMENT '每个小时计划生产产品数',
  `minute_qty` int NULL DEFAULT 0 COMMENT '每分钟计划生产产品数',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `is_deleted` tinyint NOT NULL DEFAULT 0 COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `afternoon_start` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下午开始时间 默认  下午一点',
  `afternoon_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下午结束时间',
  `evening_start` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夜班开始时间',
  `evening_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夜班结束时间',
  `morning_start` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '早班开始时间',
  `morning_end` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '早班结束时间',
  `last_time` datetime NULL DEFAULT NULL COMMENT '最近一件产品生产时间',
  `expect_finish_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '理论完成率 =当日实际消耗时间/当日排产有效时间,',
  `afternoon_start1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下午1开始时间 默认  下午一点',
  `afternoon_end1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下午1结束时间',
  `evening_start1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夜班开始时间',
  `evening_end1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '夜班结束时间',
  `morning_start1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '早班开始时间',
  `morning_end1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '早班结束时间',
  `group_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班组',
  `line_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生产线名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_daily_plan
-- ----------------------------
INSERT INTO `sp_daily_plan` VALUES ('1815271482298601474', 'PLAN20240722-1', 'DD2024072201', '2024-07-22 14:23:00', 30, 33, 1000, 400, 0000000002, 40.00, 99.50, NULL, 0, '2024-07-22 14:23:42', 'admin', '2024-08-12 01:09:37', 'admin', 0, '2024-07-22 13:00:00', '2024-07-22 15:10:00', NULL, NULL, '2024-07-22 08:00:00', '2024-07-22 10:00:00', '2024-07-22 14:42:06', 0.00, '2024-07-22 15:00:00', '2024-07-22 17:40:00', NULL, NULL, '2024-07-22 10:10:00', '2024-07-22 11:40:00', '磁阻2线1班', '磁阻传感器');
INSERT INTO `sp_daily_plan` VALUES ('1821893578122477569', 'PLAN2024080901', 'DD2024080901', '2024-08-09 08:00:00', 30, 21, 500, 450, 0000000002, 90.00, 99.56, NULL, 0, '2024-08-09 20:57:33', 'admin', '2024-08-12 00:59:25', 'admin', 0, '2024-08-09 13:00:00', '2024-08-09 15:10:00', NULL, NULL, '2024-08-09 08:00:00', '2024-08-09 10:00:00', NULL, 0.00, '2024-08-09 15:00:00', '2024-08-09 17:40:00', NULL, NULL, '2024-08-09 10:10:00', '2024-08-09 11:40:00', '测速传感器', '测速传感器');
INSERT INTO `sp_daily_plan` VALUES ('1822068374718357506', 'PLAN2024080902', 'DD2024080902', '2024-08-10 08:31:00', 30, 66, 500, 200, 0000000003, 40.00, 98.50, NULL, 0, '2024-08-10 08:32:08', 'admin', '2024-08-12 01:01:01', 'admin', 0, '2024-08-10 13:00:00', '2024-08-10 15:10:00', NULL, NULL, '2024-08-10 08:00:00', '2024-08-10 10:00:00', NULL, 0.00, '2024-08-10 15:00:00', '2024-08-10 17:40:00', NULL, NULL, '2024-08-10 10:10:00', '2024-08-10 11:40:00', '压力传感器1班', '压力传感器');
INSERT INTO `sp_daily_plan` VALUES ('1822682583742296066', 'PLAN2024080902-1', 'DD2024080902', '2024-08-10 01:12:00', 30, 27, 500, 200, 0000000001, 40.00, 99.50, NULL, 0, '2024-08-12 01:12:46', 'admin', '2024-08-12 01:42:11', 'admin', 0, '2024-08-10 13:00:00', '2024-08-10 15:10:00', NULL, NULL, '2024-08-10 08:00:00', '2024-08-10 10:00:00', NULL, 0.00, '2024-08-10 15:00:00', '2024-08-10 17:40:00', NULL, NULL, '2024-08-10 10:10:00', '2024-08-10 11:40:00', '压力传感器1班', '压力传感器');

-- ----------------------------
-- Table structure for sp_device
-- ----------------------------
DROP TABLE IF EXISTS `sp_device`;
CREATE TABLE `sp_device`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `device` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备名称',
  `device_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备描述',
  `supplier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '供应商',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `flag` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '设备状态: 0正常运行，1故障，2维修中 3 未启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_device
-- ----------------------------
INSERT INTO `sp_device` VALUES ('1811034635520602113', 'BM001', '', '', '2024-07-10 21:47:59', 'admin', '2024-07-11 09:56:01', 'admin', '0', '2');
INSERT INTO `sp_device` VALUES ('1811034858133286914', 'BM002', '', '', '2024-07-10 21:48:52', 'admin', '2024-07-10 21:48:52', 'admin', '0', '0');
INSERT INTO `sp_device` VALUES ('1811034911006683137', 'BM003', '', '', '2024-07-10 21:49:05', 'admin', '2024-07-10 21:49:05', 'admin', '0', '0');
INSERT INTO `sp_device` VALUES ('1811034927846813697', 'BM003', '', '', '2024-07-10 21:49:09', 'admin', '2024-07-10 21:49:09', 'admin', '0', '0');

-- ----------------------------
-- Table structure for sp_device_activation
-- ----------------------------
DROP TABLE IF EXISTS `sp_device_activation`;
CREATE TABLE `sp_device_activation`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `device` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备名称',
  `supplier` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '供应商',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `plan_time` int NULL DEFAULT 480 COMMENT '计划负载时长(已经扣除中间正常休息时间)',
  `work_time` int NULL DEFAULT 1 COMMENT '实际负载时长=plan_time-bad_time-wait_time',
  `bad_time` int NULL DEFAULT 0 COMMENT '故障时长(统计当日的,从维修信息表中统计)',
  `activation_rate` float(5, 2) NULL DEFAULT 100.00 COMMENT '设备稼动率=work_time/plan_time  %100',
  `wait_time` int NULL DEFAULT 0 COMMENT '等待时长(统计当日的,这个 不好统计)',
  `flag` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '设备状态: 0正常运行，1故障，2维修中 3 未启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_device_activation
-- ----------------------------
INSERT INTO `sp_device_activation` VALUES ('1559898665796165633', '罗盘-01', '华夏智造', '2022-08-17 21:43:18', 'admin', '2023-12-04 15:05:06', 'admin', '0', 480, 4801, 0, 100.00, 0, '0');
INSERT INTO `sp_device_activation` VALUES ('1559905357585858561', 'SMT-01', '富士康', '2022-08-17 22:09:54', 'admin', '2022-08-18 11:44:03', 'admin', '0', 480, 380, 30, 80.00, NULL, '0');

-- ----------------------------
-- Table structure for sp_device_maintain
-- ----------------------------
DROP TABLE IF EXISTS `sp_device_maintain`;
CREATE TABLE `sp_device_maintain`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '主键id',
  `device` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备名称',
  `error_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '故障描述',
  `dealer` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '处理者',
  `result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '处理结果',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `flag` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '0正常运行，1故障，2维修中 3 禁用',
  `bad_time` int NULL DEFAULT 0 COMMENT '本次故障时长(分钟)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_device_maintain
-- ----------------------------
INSERT INTO `sp_device_maintain` VALUES ('1560079314595784263', '绕线机01', '缺线', 'admin', 'ok ', '2022-08-18 09:41:08', 'admin', '2024-08-12 00:56:07', 'admin', '0', '故障', 5);
INSERT INTO `sp_device_maintain` VALUES ('1731594158925406210', 'test', '电源温度超高', '', '', '2023-12-04 16:39:54', 'admin', '2024-08-12 00:56:35', 'admin', '0', '维修中', 0);

-- ----------------------------
-- Table structure for sp_employee
-- ----------------------------
DROP TABLE IF EXISTS `sp_employee`;
CREATE TABLE `sp_employee`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `employee_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `group_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属班组',
  `im` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT 'im 通讯id，可用于下达工单发送消息',
  `contact` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系方式',
  `image` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图像url',
  `gender` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `department` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属部门',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_employee
-- ----------------------------
INSERT INTO `sp_employee` VALUES ('1575516389723148290', '张一鸣', '磁阻2线1班', 'none', '18865302636', NULL, '男', NULL, NULL, '2022-09-30 00:02:34', 'admin', '2024-07-14 20:31:12', 'admin', '0');
INSERT INTO `sp_employee` VALUES ('1601472979955585025', '赵狄', '磁阻2线1班', 'wx_afa3434234', '18665802636', NULL, '', NULL, NULL, '2022-12-10 15:04:47', 'admin', '2022-12-10 15:04:47', 'admin', '0');

-- ----------------------------
-- Table structure for sp_factroy
-- ----------------------------
DROP TABLE IF EXISTS `sp_factroy`;
CREATE TABLE `sp_factroy`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `factory` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `factory_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工厂表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_factroy
-- ----------------------------
INSERT INTO `sp_factroy` VALUES ('1336542027055136', 'dream-shop', 'www.dreammm.net', '2020-03-12 15:22:02', 'admin', '2020-03-13 10:15:54', 'admin');
INSERT INTO `sp_factroy` VALUES ('1336542027089009', 'hme-shop', 'www.dreammm.net', '2020-03-12 15:22:02', 'admin', '2020-03-13 10:15:54', 'admin');

-- ----------------------------
-- Table structure for sp_flow
-- ----------------------------
DROP TABLE IF EXISTS `sp_flow`;
CREATE TABLE `sp_flow`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `flow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '流程',
  `flow_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '线体描述',
  `process` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程绘制 A——>B——>C',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '流程表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_flow
-- ----------------------------
INSERT INTO `sp_flow` VALUES ('1277600512544583681', '测速传感器', '测速传感器', '上料->贴片->绕丝->加固->打码->入库->测试', '2020-06-29 21:51:14', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow` VALUES ('1559443376458342402', '磁阻传感器', '磁阻传感器', '上料->贴片->绕丝->加固->测试->打码', '2022-08-16 15:34:09', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow` VALUES ('1822063427582926849', '压力传感器', '压力传感器', '上料->贴片->绕丝->加固->测试->打码', '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');

-- ----------------------------
-- Table structure for sp_flow_oper_relation
-- ----------------------------
DROP TABLE IF EXISTS `sp_flow_oper_relation`;
CREATE TABLE `sp_flow_oper_relation`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `flow_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '流程ID',
  `flow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '流程代码',
  `per_oper_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '前道工序ID',
  `per_oper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '前道工序代码',
  `oper_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '当前工序ID',
  `oper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '当前工序\r\n',
  `next_oper_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下道工序ID',
  `next_oper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '下道工序',
  `sort_num` int NOT NULL COMMENT '排序',
  `oper_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工序类型（首道工序firstOper;最后一道工序lastOper）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `flow_id_index`(`flow_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '流程与工序关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_flow_oper_relation
-- ----------------------------
INSERT INTO `sp_flow_oper_relation` VALUES ('1275430361636253697', '1275430361590116354', '002', '', '', '1336864489340960', 'ASY-01', '1336864575324192', 'APK-01', 1, NULL, '2020-06-23 22:07:49', 'admin', '2020-06-23 22:07:49', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1275430361636253698', '1275430361590116354', '002', '1336864489340960', 'ASY-01', '1336864575324192', 'APK-01', '', '', 2, NULL, '2020-06-23 22:07:49', 'admin', '2020-06-23 22:07:49', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1278145622248239105', '1278145622063689729', '1212', '', '', '1336864489340960', 'ASY-01', '1336864575324192', 'APK-01', 1, NULL, '2020-07-01 09:57:18', 'admin', '2020-07-01 09:57:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1278145622248239106', '1278145622063689729', '1212', '1336864489340960', 'ASY-01', '1336864575324192', 'APK-01', '', '', 2, NULL, '2020-07-01 09:57:18', 'admin', '2020-07-01 09:57:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765762', '1277600512544583681', '测速传感器', '', '', '1559594964397264897', '上料', '1559595062946631682', '贴片', 1, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765763', '1277600512544583681', '测速传感器', '1559594964397264897', '上料', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', 2, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765764', '1277600512544583681', '测速传感器', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', '1559595327984701441', '加固', 3, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765765', '1277600512544583681', '测速传感器', '1559595229389197314', '绕丝', '1559595327984701441', '加固', '1559595533094555650', '打码', 4, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765766', '1277600512544583681', '测速传感器', '1559595327984701441', '加固', '1559595533094555650', '打码', '1866580263623456', '入库', 5, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765767', '1277600512544583681', '测速传感器', '1559595533094555650', '打码', '1866580263623456', '入库', '1559595389871656962', '测试', 6, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056539499765768', '1277600512544583681', '测速传感器', '1866580263623456', '入库', '1559595389871656962', '测试', '', '', 7, NULL, '2024-08-10 07:45:06', 'admin', '2024-08-10 07:45:06', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996162', '1559443376458342402', '磁阻传感器', '', '', '1559594964397264897', '上料', '1559595062946631682', '贴片', 1, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996163', '1559443376458342402', '磁阻传感器', '1559594964397264897', '上料', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', 2, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996164', '1559443376458342402', '磁阻传感器', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', '1559595327984701441', '加固', 3, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996165', '1559443376458342402', '磁阻传感器', '1559595229389197314', '绕丝', '1559595327984701441', '加固', '1559595389871656962', '测试', 4, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996166', '1559443376458342402', '磁阻传感器', '1559595327984701441', '加固', '1559595389871656962', '测试', '1559595533094555650', '打码', 5, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822056592347996167', '1559443376458342402', '磁阻传感器', '1559595389871656962', '测试', '1559595533094555650', '打码', '', '', 6, NULL, '2024-08-10 07:45:18', 'admin', '2024-08-10 07:45:18', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035714', '1822063427582926849', '压力传感器', '', '', '1559594964397264897', '上料', '1559595062946631682', '贴片', 1, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035715', '1822063427582926849', '压力传感器', '1559594964397264897', '上料', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', 2, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035716', '1822063427582926849', '压力传感器', '1559595062946631682', '贴片', '1559595229389197314', '绕丝', '1559595327984701441', '加固', 3, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035717', '1822063427582926849', '压力传感器', '1559595229389197314', '绕丝', '1559595327984701441', '加固', '1559595389871656962', '测试', 4, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035718', '1822063427582926849', '压力传感器', '1559595327984701441', '加固', '1559595389871656962', '测试', '1559595533094555650', '打码', 5, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');
INSERT INTO `sp_flow_oper_relation` VALUES ('1822063427650035719', '1822063427582926849', '压力传感器', '1559595389871656962', '测试', '1559595533094555650', '打码', '', '', 6, NULL, '2024-08-10 08:12:28', 'admin', '2024-08-10 08:12:28', 'admin');

-- ----------------------------
-- Table structure for sp_global_id
-- ----------------------------
DROP TABLE IF EXISTS `sp_global_id`;
CREATE TABLE `sp_global_id`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `order_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '全局订单编号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `line` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生产线',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '线体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_global_id
-- ----------------------------
INSERT INTO `sp_global_id` VALUES ('1582223019770830849', 'DD20240722', '2022-10-18 12:12:19', 'admin', '2024-07-22 14:23:54', 'admin', '磁阻传感器');
INSERT INTO `sp_global_id` VALUES ('1582225947189501953', 'feeee', '2022-10-18 12:23:57', 'admin', '2022-10-18 16:58:59', 'admin', '测速传感器');

-- ----------------------------
-- Table structure for sp_global_setting
-- ----------------------------
DROP TABLE IF EXISTS `sp_global_setting`;
CREATE TABLE `sp_global_setting`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `fronturl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '看板URL',
  `rabbitmqurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'rabbitmq URL',
  `line` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生产线',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '线体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_global_setting
-- ----------------------------
INSERT INTO `sp_global_setting` VALUES ('1336867983196192', 'http://127.0.0.1:8081', 'http://192.168.0.24:15672/#', '磁阻变压器', '2022-07-24 10:32:10', 'admin', '2022-09-28 20:41:05', 'admin');

-- ----------------------------
-- Table structure for sp_group
-- ----------------------------
DROP TABLE IF EXISTS `sp_group`;
CREATE TABLE `sp_group`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班组名称',
  `leader` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班长',
  `group_count` int NULL DEFAULT 0 COMMENT '班组成员数量',
  `contact` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系方式',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_group
-- ----------------------------
INSERT INTO `sp_group` VALUES ('1564530019699695618', '磁阻2线1班', '吴义迪', 4, '13714501649', '2022-08-30 16:26:39', 'admin', '2024-08-09 19:28:03', 'admin', '0');
INSERT INTO `sp_group` VALUES ('1564530510630395906', '压力传感器1班', '马龙', 4, '18665802636', '2022-08-30 16:28:36', 'admin', '2024-08-12 03:10:10', 'admin', '0');
INSERT INTO `sp_group` VALUES ('1564538664114085889', '加速度传感器1号线', '赖小铭', 5, '18864476599', '2022-08-30 17:01:00', 'admin', '2024-08-12 00:20:28', 'admin', '0');
INSERT INTO `sp_group` VALUES ('1821893734821675009', '测速传感器', '陈梦', 4, '18665802636', '2024-08-09 20:58:10', 'admin', '2024-08-09 20:58:10', 'admin', '0');

-- ----------------------------
-- Table structure for sp_line
-- ----------------------------
DROP TABLE IF EXISTS `sp_line`;
CREATE TABLE `sp_line`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `line` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '生产线',
  `line_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '线体描述',
  `process_section` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工序段代号',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '线体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_line
-- ----------------------------
INSERT INTO `sp_line` VALUES ('1336867983196192', 'HME-GCZ-01', '磁阻变压器', '从vv', '2020-03-14 10:32:10', 'admin', '2020-06-14 02:20:09', 'admin');
INSERT INTO `sp_line` VALUES ('1336868041916448', 'HME-CE-01', '测速传感器产线', 'TST', '2020-03-14 10:32:38', 'admin', '2020-03-14 10:32:38', 'admin');
INSERT INTO `sp_line` VALUES ('1336868662673440', 'HME-YL01', '压力传感器', 'ASY', '2020-03-14 10:37:34', 'admin', '2020-06-16 11:47:04', 'admin');

-- ----------------------------
-- Table structure for sp_material_fetch
-- ----------------------------
DROP TABLE IF EXISTS `sp_material_fetch`;
CREATE TABLE `sp_material_fetch`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `material` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物料编码',
  `batch_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '批次',
  `amount` int NULL DEFAULT NULL COMMENT '数量',
  `unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料单位',
  `handler` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '领料人',
  `house_out` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '调出仓库',
  `house_in` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '调入仓库',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `is_deleted` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '基础物料表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_material_fetch
-- ----------------------------
INSERT INTO `sp_material_fetch` VALUES ('1588535536579186689', 'PCB101000037', '220610', 1000, '件', '赵狄', '主仓库', '一车间', '2022-11-04 22:16:00', 'admin', '2022-12-10 14:06:01', 'admin', '0');
INSERT INTO `sp_material_fetch` VALUES ('1588535536969256961', 'THREAD001001', '220927', 1000, '件', '赵狄', '主仓库', '一车间', '2022-11-04 22:16:00', 'admin', '2022-12-10 23:46:12', 'admin', '0');
INSERT INTO `sp_material_fetch` VALUES ('1822065584742834178', 'DIPIAN', '2024080901', 1000, 'pcs', '马龙', '主仓库', '一车间', '2024-08-10 08:21:02', 'admin', '2024-08-10 08:21:35', 'admin', '0');

-- ----------------------------
-- Table structure for sp_material_trace
-- ----------------------------
DROP TABLE IF EXISTS `sp_material_trace`;
CREATE TABLE `sp_material_trace`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
  `product_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '产品编码',
  `product_serial` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品序列号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品名称',
  `bom_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'bom code',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品二维码',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单编号',
  `quality` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'ok' COMMENT '产品品质: 0 合格，1 不合格 ',
  `bad_pos` int NULL DEFAULT NULL COMMENT '不良品工位号',
  `position` int NULL DEFAULT 1 COMMENT '产品库位： 0， 车间，1 成品仓库，2 出库',
  `material` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料编码',
  `material_desc` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料描述',
  `material_batch_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料批次',
  `type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品规格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_material_trace
-- ----------------------------
INSERT INTO `sp_material_trace` VALUES ('1588711060098158593', '二十天色图', '20221025181001', NULL, 'BOM101000036', '2022-11-05 09:53:28', 'user1', '2024-07-10 16:31:58', 'admin', NULL, NULL, 'ok', NULL, 1, 'PCB101000036', NULL, '220614', 'xx-yy-zz');
INSERT INTO `sp_material_trace` VALUES ('1588711060823773186', '45x56x66.TYPE1', '20221025181101', NULL, 'BOM101000036', '2022-11-05 09:53:28', 'user1', '2022-11-05 09:53:28', 'user1', NULL, NULL, 'ok', NULL, 1, 'PCB101000036', NULL, '220614', NULL);
INSERT INTO `sp_material_trace` VALUES ('1588711061272563713', '45x56x66.TYPE1', '20221025181200', NULL, 'BOM101000036', '2022-11-05 09:53:28', 'user1', '2022-11-05 09:53:28', 'user1', NULL, NULL, 'ok', NULL, 1, 'PCB101000036', NULL, '220614', NULL);
INSERT INTO `sp_material_trace` VALUES ('1588711061599719426', '45x56x66.TYPE1', '20221025181302', NULL, 'BOM101000036', '2022-11-05 09:53:29', 'user1', '2022-12-10 16:01:19', 'admin', NULL, NULL, 'ok', NULL, 1, 'PCB101000036', NULL, '220614', '这是数据是通过设备自动采集的');

-- ----------------------------
-- Table structure for sp_materile
-- ----------------------------
DROP TABLE IF EXISTS `sp_materile`;
CREATE TABLE `sp_materile`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `materiel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物料编码',
  `materiel_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料描述',
  `qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '二维码/条形码',
  `batch_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '批次',
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '基本单位',
  `product_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品组',
  `mat_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料类型',
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '型号',
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '尺寸',
  `flow_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程',
  `flow_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程描述',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `is_deleted` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '基础物料表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_materile
-- ----------------------------
INSERT INTO `sp_materile` VALUES ('1601421267920125954', 'THREAD001001', '铜线', 'THREAD001001#220614#8000#DD20220516002', '220927', NULL, NULL, '', '', NULL, '', NULL, '2022-12-10 11:39:18', 'admin', '2024-07-19 10:15:21', 'admin', '0');
INSERT INTO `sp_materile` VALUES ('1810587657762402305', 'PCB1010000388', 'PCB1010000388', 'PCB101000037#220614#6000#DD20220516001', '220614', NULL, NULL, 'FG', '10X20', NULL, '', NULL, '2024-07-09 16:11:51', 'admin', '2024-08-10 08:14:36', 'admin', '0');
INSERT INTO `sp_materile` VALUES ('1822064328079024130', 'DIPIAN', '底片', '', '2024080901', NULL, NULL, 'FG', '2x4', NULL, '', NULL, '2024-08-10 08:16:03', 'admin', '2024-08-10 08:16:38', 'admin', '0');

-- ----------------------------
-- Table structure for sp_oper
-- ----------------------------
DROP TABLE IF EXISTS `sp_oper`;
CREATE TABLE `sp_oper`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `oper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工序\r\n',
  `oper_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工序描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工序表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_oper
-- ----------------------------
INSERT INTO `sp_oper` VALUES ('1336864489340960', 'ASY-01', '压铸', '2020-03-14 10:04:24', 'admin', '2024-07-12 16:06:01', 'admin');
INSERT INTO `sp_oper` VALUES ('1336864537575456', 'TST-02', '打毛刺', '2020-03-14 10:04:47', 'admin', '2024-07-10 21:46:49', 'admin');
INSERT INTO `sp_oper` VALUES ('1336864575324192', 'APK-01', '包装工序', '2020-03-14 10:05:05', 'admin', '2020-03-14 10:05:05', 'admin');
INSERT INTO `sp_oper` VALUES ('1336864613072928', 'TST-01', '集成测试工序', '2020-03-14 10:05:23', 'admin', '2020-03-14 10:05:23', 'admin');
INSERT INTO `sp_oper` VALUES ('1336868360683552', 'HJ-01', '焊接', '2020-03-14 10:35:10', 'admin', '2020-03-14 10:35:10', 'admin');
INSERT INTO `sp_oper` VALUES ('1336868452958240', 'FJ-01', '封胶工序', '2020-03-14 10:35:54', 'admin', '2024-07-10 16:14:40', 'admin');
INSERT INTO `sp_oper` VALUES ('1336868507484192', 'JS-01', '加酸工序', '2020-03-14 10:36:20', 'admin', '2020-03-14 10:36:20', 'admin');
INSERT INTO `sp_oper` VALUES ('1336868562010144', 'QX-01', '清洗工序', '2020-03-14 10:36:46', 'admin', '2020-03-14 10:36:46', 'admin');
INSERT INTO `sp_oper` VALUES ('1337248255574048', 'RK-01', '入库工序', '2020-03-16 12:54:18', 'admin', '2020-03-16 12:54:18', 'admin');
INSERT INTO `sp_oper` VALUES ('1559594964397264897', '上料', '上料', '2022-08-17 01:36:30', 'admin', '2022-08-17 01:36:30', 'admin');
INSERT INTO `sp_oper` VALUES ('1559595062946631682', '贴片', '贴片', '2022-08-17 01:36:54', 'admin', '2022-08-17 01:36:54', 'admin');
INSERT INTO `sp_oper` VALUES ('1559595229389197314', '绕丝', '绕丝', '2022-08-17 01:37:33', 'admin', '2022-08-17 01:37:33', 'admin');
INSERT INTO `sp_oper` VALUES ('1559595327984701441', '加固', '加固', '2022-08-17 01:37:57', 'admin', '2022-08-17 01:37:57', 'admin');
INSERT INTO `sp_oper` VALUES ('1559595389871656962', '测试', '测试', '2022-08-17 01:38:11', 'admin', '2022-08-17 01:38:11', 'admin');
INSERT INTO `sp_oper` VALUES ('1559595533094555650', '打码', '打码', '2022-08-17 01:38:46', 'admin', '2022-08-17 01:38:46', 'admin');
INSERT INTO `sp_oper` VALUES ('1866580263623456', '入库', '入库', '2022-06-23 14:41:41', 'admin', '2024-08-10 07:42:52', 'admin');

-- ----------------------------
-- Table structure for sp_order
-- ----------------------------
DROP TABLE IF EXISTS `sp_order`;
CREATE TABLE `sp_order`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0.00' COMMENT '主键id',
  `order_code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '工单编号',
  `order_description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '工单描述',
  `plan_qty` int UNSIGNED NULL DEFAULT 0 COMMENT '计划产品数量',
  `order_type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '订单类型 P 量产 A验证 F返工 ',
  `flow` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '工艺流程',
  `product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品类型',
  `plan_start_time` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '计划开始时间',
  `plan_end_time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '计划结束时间',
  `status` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '创建' COMMENT '1,创建 2 进行中，3订单结束，4订单终结',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `line` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生产线',
  `maked_qty` int NULL DEFAULT 0 COMMENT '已经生产数量',
  `bad_qty` int(6) UNSIGNED ZEROFILL NULL DEFAULT 000000 COMMENT '不合格数量',
  `pass_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '质检通过率',
  `qrcode` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '订单二维码,可以从二维码解析出多个字段，依赖于客户的具体需求，保留字段',
  `memo` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '备注',
  `finish_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '订单完成率',
  `bom` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'bom id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_order
-- ----------------------------
INSERT INTO `sp_order` VALUES ('1588681873509490689', 'DD2024072201', NULL, 3000, NULL, '磁阻传感器', NULL, '2024-07-22 07:57:00', '2024-07-30 09:01:00', '进行中', '2022-11-05 07:57:30', 'admin', '2024-08-12 01:09:37', 'admin', NULL, 400, 000002, 99.50, '', '系统自动报工，自动生成产品信息', 13.33, '磁阻传感器');
INSERT INTO `sp_order` VALUES ('1821893212098150401', 'DD2024080901', NULL, 1000, NULL, '测速传感器', NULL, '2024-08-09 20:55:00', '2024-08-12 20:55:00', '进行中', '2024-08-09 20:56:06', 'admin', '2024-08-12 00:59:25', 'admin', NULL, 450, 000002, 99.56, '', '', 45.00, '测速传感器');
INSERT INTO `sp_order` VALUES ('1822063766302334978', 'DD2024080902', NULL, 1000, NULL, '压力传感器', NULL, '2024-08-10 08:13:00', '2024-08-11 08:13:00', '进行中', '2024-08-10 08:13:49', 'admin', '2024-08-12 01:42:11', 'admin', NULL, 400, 000004, 99.00, '', '', 40.00, '压力传感器');

-- ----------------------------
-- Table structure for sp_order_material
-- ----------------------------
DROP TABLE IF EXISTS `sp_order_material`;
CREATE TABLE `sp_order_material`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `order_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工单编码',
  `bom_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'BOM编码',
  `line` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生产线',
  `material_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '物料编码',
  `amount` int NULL DEFAULT NULL COMMENT '数量',
  `batch_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '批次',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `real_amount` int NULL DEFAULT NULL COMMENT '实际数量',
  `remain_amount` int NULL DEFAULT NULL COMMENT '剩余数量=amount-real_amount',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '线体表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_order_material
-- ----------------------------
INSERT INTO `sp_order_material` VALUES ('1588539587207303169', 'DD2022070202', '12321', '磁阻传感器', 'EXY101000036', 600, '221001', '2022-11-04 22:32:06', 'admin', '2024-07-10 10:11:04', 'admin', NULL, NULL);
INSERT INTO `sp_order_material` VALUES ('1731584302906597378', '20231204', '12321', '测速传感器', 'PCB101000037', 10, '220610', '2023-12-04 16:00:44', 'admin', '2023-12-04 16:00:44', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for sp_product
-- ----------------------------
DROP TABLE IF EXISTS `sp_product`;
CREATE TABLE `sp_product`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
  `product_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '产品编码',
  `serial_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品序列号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品名称',
  `bom_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'bom code',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品二维码',
  `batch_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品批次',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单编号',
  `quality` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'ok' COMMENT '产品品质: 0 合格，1 不合格 ',
  `bad_pos` int NULL DEFAULT NULL COMMENT '不良品工位号',
  `position` int NULL DEFAULT 1 COMMENT '产品库位： 0， 车间，1 成品仓库，2 出库',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `flow_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程id',
  `flow_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '流程描述',
  `type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '产品规格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_product
-- ----------------------------
INSERT INTO `sp_product` VALUES ('1588684186349355009', '钛阳极', '202281813146', '', '', '2022-11-05 08:06:41', 'lgl', '2024-07-10 16:31:44', 'admin', '产品数据是从设备采集自动生成的', '', 'DD2022110501', 'OK', 0, 1, '0', '1277176874674663425', 'A01装配工序->测试工序', '');
INSERT INTO `sp_product` VALUES ('1588684343593811970', '37X46Y568.TEST', '202281813136', '', '', '2022-11-05 08:07:19', 'lgl', '2024-07-10 16:32:04', 'admin', '', '', 'DD2022110501', 'OK', 0, 1, '0', '1559443376458342402', '磁阻传感器', '');
INSERT INTO `sp_product` VALUES ('1588684521612656642', '37X46Y568.TEST', '202281813126', NULL, NULL, '2022-11-05 08:08:01', 'lgl', '2022-11-05 08:08:01', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588684601925189633', '37X46Y568.TEST', '202281813122', NULL, NULL, '2022-11-05 08:08:20', 'lgl', '2022-11-05 08:08:20', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588686318809006082', '37X46Y568.TEST', '202281813046', NULL, NULL, '2022-11-05 08:15:09', 'lgl', '2022-11-05 08:15:09', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588686494298685442', '37X46Y568.TEST', '202281813145', NULL, NULL, '2022-11-05 08:15:51', 'lgl', '2022-11-05 08:15:51', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588695338538123266', '37X46Y568.TEST', '202281813147', NULL, NULL, '2022-11-05 08:51:00', 'lgl', '2022-11-05 08:51:00', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588697191590055937', '37X46Y568.TEST', '202281813148', '', '', '2022-11-05 08:58:22', 'lgl', '2024-07-12 17:56:38', 'admin', '', '', 'DD2022110501', 'NG', 2, 1, '0', '1277600512544583681', '测速传感器', '');
INSERT INTO `sp_product` VALUES ('1588706243984416769', '37X46Y568.TEST', '202281813149', NULL, NULL, '2022-11-05 09:34:20', 'lgl', '2022-11-05 09:34:20', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1588707210758905857', '37X46Y568.TEST', '20228181314@', NULL, NULL, '2022-11-05 09:38:10', 'lgl', '2022-11-05 09:38:10', 'lgl', NULL, NULL, 'DD2022110501', 'OK', 0, 1, '0', '磁阻传感器', NULL, NULL);
INSERT INTO `sp_product` VALUES ('1815233826776891393', 'lgl-test-测试', 'lgl-test-测试01', '', '', '2024-07-22 11:54:04', 'admin', '2024-07-22 11:54:04', 'admin', '', '', '', '', NULL, 1, '0', '1559443376458342402', '磁阻传感器', '');
INSERT INTO `sp_product` VALUES ('1815272088056762370', 'whyfailed', 'whyfailed-serial', '', '', '2024-07-22 14:26:07', 'admin', '2024-07-22 14:26:07', 'admin', '', '', '', '', NULL, 1, '0', '1559443376458342402', '磁阻传感器', '');
INSERT INTO `sp_product` VALUES ('1815272264418856962', 'whyfailed01', 'whyfailed-serial01', '', '', '2024-07-22 14:26:49', 'admin', '2024-07-22 14:26:49', 'admin', '', '', '', '', NULL, 1, '0', '1559443376458342402', '磁阻传感器', '');
INSERT INTO `sp_product` VALUES ('1815276110641872897', 'hello', 'hello0--01', '', '', '2024-07-22 14:42:06', 'admin', '2024-07-22 14:42:06', 'admin', '', '', 'DD20240722', 'NG', NULL, 1, '0', '1559443376458342402', '磁阻传感器', '');

-- ----------------------------
-- Table structure for sp_sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_department`;
CREATE TABLE `sp_sys_department`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `parent_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sort_num` int NOT NULL,
  `is_deleted` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_department
-- ----------------------------

-- ----------------------------
-- Table structure for sp_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_dict`;
CREATE TABLE `sp_sys_dict`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标签名',
  `value` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '数据值',
  `type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型',
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '描述',
  `sort_num` int NOT NULL COMMENT '排序（升序）',
  `parent_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '父级id',
  `is_deleted` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sp_sys_dict_name`(`type` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统字典表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_dict
-- ----------------------------
INSERT INTO `sp_sys_dict` VALUES ('1337618042191904', '成品', 'FG', 'material_type', '物料类型', 2, '\"\"', '0', '2020-03-18 13:53:06', 'admin', '2020-03-18 13:53:06', 'admin');
INSERT INTO `sp_sys_dict` VALUES ('1337618163826720', '半成品', 'PG', 'material_type', '物料类型', 3, '\"\"', '0', '2020-03-18 13:54:04', 'admin', '2020-03-18 13:54:04', 'admin');
INSERT INTO `sp_sys_dict` VALUES ('1337618837012512', '个', 'PCS', 'ORDER_UNIT', '生产单位', 1, '\"\"', '0', '2020-03-18 13:59:25', 'admin', '2020-03-18 13:59:41', 'admin');
INSERT INTO `sp_sys_dict` VALUES ('1337618939772960', '箱', 'BOX', 'ORDER_UNIT', '生产单位', 2, '\"\"', '0', '2020-03-18 14:00:14', 'admin', '2020-03-18 14:00:14', 'admin');

-- ----------------------------
-- Table structure for sp_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_log`;
CREATE TABLE `sp_sys_log`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `user_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '操作',
  `method` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '方法',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '参数',
  `time` bigint NULL DEFAULT NULL COMMENT '执行时长（毫秒）',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'ip地址',
  `remarks` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '所属租户',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标识 0未删除，1已删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '日志 ' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sp_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_menu`;
CREATE TABLE `sp_sys_menu`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单URL',
  `parent_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '父菜单ID，一级菜单设为0',
  `grade` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '层级：1级、2级、3级......',
  `sort_num` int NOT NULL COMMENT '排序',
  `type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型：0 目录；1 菜单；2 按钮',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '授权(多个用逗号分隔，如：sys:menu:list,sys:menu:create)',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '菜单图标',
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_sp_sys_menu_name`(`name` ASC) USING BTREE,
  UNIQUE INDEX `idx_sp_sys_menu_code`(`code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_menu
-- ----------------------------
INSERT INTO `sp_sys_menu` VALUES ('10', 'system', '系统管理', '#', '7', '2', 1, '0', 'user:add', 'fa fa-gears', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('101', 'menu', '菜单管理', '/admin/sys/menu/list-ui', '10', '3', 1, '0', 'user:add', 'fa fa-bars', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('102', 'user', '用户管理', '/admin/sys/user/list-ui', '10', '3', 2, '0', 'user:add', 'fa fa-user', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('103', 'role', '角色管理', '/admin/sys/role/list-ui', '10', '3', 3, '0', 'user:add', 'fa fa-child', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('104', 'department', '部门管理', '/admin/sys/department/list-ui', '10000', '3', 4, '0', 'user:add', 'fa fa-sitemap', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('105', 'basedata', '基础数据配置平台', '/basedata/manager/list-ui', '10', '3', 5, '0', 'user:add', 'fa fa-cog', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('106', 'basedatamanager', '基础数据维护', '/basedata/manager/item/list-ui', '10', '3', 6, '0', 'user:add', 'fa fa-database', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('107', 'log', '日志管理', '/log/list-ui', '10', '3', 2, '0', 'user:add', 'fa fa-file-archive-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('12', 'order', '生产管理', '', '7', '2', 4, '0', 'user:add', 'fa fa-calendar', '', '2022-06-29 11:18:29', 'dreamer', '2024-08-10 08:23:00', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('121', 'orderList', '工单台账', '/order/list-ui', '12', '3', 1, '0', 'user:add', 'fa fa-retweet', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('122', 'orderRelease', '工单甘特图', '/order/gant-list-ui', '12', '3', 1, '0', 'user:add', 'fa fa-flag-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('123', 'orderToday', '每日计划', '/daily/plan/list-ui', '12', '3', 1, '0', 'user:add', 'fa fa-clock-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('124', 'workreport', '报工', '/workReport/list-ui', '12', '3', 4, '0', 'user:add', 'fa fa-lemon-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('13', 'materiel', '物料管理', '#', '7', '2', 2, '0', 'user:add', 'fa fa-cubes', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('131', 'matdef', '物料台账', '/basedata/materile/list-ui', '13', '3', 1, '0', 'user:add', 'fa fa-microchip', '', '2022-06-29 11:18:29', 'dreamer', '2024-07-10 16:06:11', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('132', 'orderMaterial', '订单物料计划', '/order/orderMaterial/list-ui', '13', '3', 1, '0', 'user:add', 'fa fa-ravelry', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('133', 'materialFetch', '物料领取', '/material/fetch/list-ui', '13', '3', 1, '0', 'user:add', 'fa fa-file-text-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('14', 'Digitalplatform\n\n', '可视化平台', '#', '7', '2', 6, '0', 'user:add', 'fa fa-camera ', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('142', 'bigscreen2', '智慧大屏', '/admin/welcome-ui', '14', '3', 3, '0', 'user:add', 'fa fa-bar-chart ', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('143', 'bigscreen1', '  智能车间1', 'http://47.242.184.27:8081', '14', '3', 2, '0', 'user:add', 'fa fa-bar-chart ', '', '2022-06-29 11:18:29', 'dreamer', '2024-08-11 18:41:28', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('144', 'bigscreen4', '  智能车间2', 'http://47.242.184.27:8082', '14', '3', 3, '0', 'user:add', 'fa fa-bar-chart ', '', '2022-06-29 11:18:29', 'dreamer', '2024-08-11 18:41:28', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('15', 'ProcessManage', '工艺和工序', '#', '7', '2', 3, '0', 'user:add', 'fa fa-wrench', '', '2022-06-29 11:18:29', 'dreamer', '2022-07-15 01:19:46', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('151', 'flowProcess', '工艺路线', '/basedata/flow/process/list-ui', '15', '3', 1, '0', 'user:add', 'fa fa-retweet', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('152', 'bom', 'BOM', '/technology/bom/list-ui', '13', '3', 2, '0', 'user:add', 'fa fa-file-text-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('153', 'operation', '工序管理', '/basedata/operation/list-ui', '15', '3', 4, '0', 'user:add', 'fa fa-cog', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('16', 'product', '产品管理', '#', '7', '2', 5, '0', 'user:add', 'fa fa-industry', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('161', 'generalSnProcess', 'SN通用过程采集', '/generalSnProcess', '16000', '3', 1, '0', 'user:add', 'fa fa-product-hunt', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('162', 'productProcess', '产品维护', '/product/list-ui', '16', '3', 1, '0', 'user:add', 'fa fa-microchip', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('163', 'materialTrace', '产品溯源', '/trace/list-ui', '16', '3', 1, '0', 'user:add', 'fa fa-lemon-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('17', 'DigitalSimulation', '数字孪生', '#', '10000', '2', 7, '0', 'user:add', 'fa fa-ravelry', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('171', 'DigitalSimulationFrom', '数字仿真3D仓库', '/digital/simulation/list-ui', '17000', '3', 1, '0', 'user:add', 'fa fa-codepen', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('18', 'device', '设备管理', '#', '7', '2', 5, '0', 'user:add', 'fa fa-address-book', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('181', 'deviceAcount', '设备台账', '/device/device/list-ui', '18', '3', 1, '0', 'user:add', 'fa fa-retweet', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('182', 'deviceMaintan', '设备维护', '/device/maintain/list-ui', '18', '3', 2, '0', 'user:add', 'fa fa-codepen', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('183', 'deviceAcativation', '时间稼动率', '/device/activation/list-ui', '18', '3', 3, '0', 'user:add', 'fa fa-wrench', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('19', 'grup', '班组管理', '#', '7', '2', 5, '1', 'user:add', 'fa fa-desktop', '', '2022-06-29 11:18:29', 'dreamer', '2024-07-16 16:27:27', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('191', 'group-list', '班组台账', '/group/list-ui', '19', '3', 1, '0', 'user:add', 'layui-icon-user', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('192', 'employee-list', '产线员工', '/employee/list-ui', '19', '3', 1, '0', 'user:add', 'fa fa-address-card', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('21', 'datCollect', '数据采集', '#', '8', '2', 1, '0', 'user:add', 'fa fa-database', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('211', 'dataollectConfig', '数据采集配置', '/config/list-ui', '21', '3', 1, '0', 'user:add', 'fa fa-flag-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('22', 'mq', '消息队列', '#', '8', '2', 1, '0', 'user:add', 'fa fa-cubes', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('221', 'rabbitmq', 'rabbitmq测试', '/rabbitmq/demo', '22', '3', 1, '0', 'user:add', 'fa fa-flag-o', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('222', 'rabbitmq-admin', 'Rabbitmq管理', '/rabbitmq/admin', '22', '3', 1, '0', 'user:add', 'fa fa-gears', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');
INSERT INTO `sp_sys_menu` VALUES ('7', 'currency', '常规管理', '#', '0', '1', 1, '0', 'user:add', 'fa fa-address-book', '', '2022-05-30 11:18:29', 'admin', '2024-07-18 18:01:48', 'admin');
INSERT INTO `sp_sys_menu` VALUES ('8', 'component', '现场数据采集', '#', '0', '1', 1, '0', 'user:add', 'fa fa-database', '', '2022-06-29 11:18:29', 'dreamer', '2019-10-18 11:18:29', 'dreamer');

-- ----------------------------
-- Table structure for sp_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_role`;
CREATE TABLE `sp_sys_role`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色编码',
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '角色描述',
  `is_deleted` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_sp_sys_role_name`(`name` ASC) USING BTREE,
  UNIQUE INDEX `idx_sp_sys_role_code`(`code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_role
-- ----------------------------
INSERT INTO `sp_sys_role` VALUES ('1185025876737396738', '超级管理员', 'admin', '超级管理员', '0', '2020-10-18 10:52:40', 'dremer', '2020-03-13 14:06:43', 'admin');
INSERT INTO `sp_sys_role` VALUES ('1232532514523213826', '刚', '质检', '体验者', '0', '2020-02-26 13:07:05', 'admin', '2024-07-18 18:02:17', 'admin');
INSERT INTO `sp_sys_role` VALUES ('1730406078675673090', '户', '采购', '体验者', '0', '2023-12-01 09:58:53', 'admin', '2023-12-04 15:37:20', 'admin');
INSERT INTO `sp_sys_role` VALUES ('1813822869552132098', '设计', '1', '1', '0', '2024-07-18 14:27:26', 'admin', '2024-07-21 13:18:35', 'admin');

-- ----------------------------
-- Table structure for sp_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_role_menu`;
CREATE TABLE `sp_sys_role_menu`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `role_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色id',
  `menu_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色对应的菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_role_menu
-- ----------------------------
INSERT INTO `sp_sys_role_menu` VALUES ('1', '1185025876737396738', '1', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('2', '1185025876737396738', '2', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('3', '1185025876737396738', '3', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('4', '1185025876737396738', '101', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('5', '1185025876737396738', '102', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('6', '1185025876737396738', '103', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');
INSERT INTO `sp_sys_role_menu` VALUES ('7', '1185025876737396738', '104', '2019-10-28 14:51:44', 'admin', '2019-10-28 14:51:56', 'admin');

-- ----------------------------
-- Table structure for sp_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_user`;
CREATE TABLE `sp_sys_user`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `username` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `dept_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '部门id',
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '邮箱',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `tel` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '固定电话',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '性别(0:女;1:男;2:其他)',
  `birthday` datetime NULL DEFAULT NULL COMMENT '出生年月日',
  `pic_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '图片id，对应sys_file表中的id',
  `id_card` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '身份证',
  `hobby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '爱好',
  `province` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '省份',
  `city` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '城市',
  `district` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '区县',
  `street` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '街道',
  `street_number` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '门牌号',
  `descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '\"\"' COMMENT '描述',
  `is_deleted` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `pwd_shadow` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码shadow',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_sp_sys_user_username`(`username` ASC) USING BTREE COMMENT '用户名唯一索引',
  UNIQUE INDEX `idx_sp_sys_user_mobile`(`mobile` ASC) USING BTREE COMMENT '用户手机号唯一索引',
  INDEX `idx_sp_sys_user_email`(`email` ASC) USING BTREE COMMENT '用户邮箱唯一索引',
  INDEX `idx_sp_sys_user_id_card`(`id_card` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_user
-- ----------------------------
INSERT INTO `sp_sys_user` VALUES ('1184019107907227640', '超级管理员', 'user', 'dd957e81b004227af3e0aa4bde869b25', '11', '2219992847@qq.com', '13714501649', '23456', '0', NULL, '55', '66', '77', '88', '99', '10', '11', '12', '13', '0', '2019-10-15 16:12:08', 'SongPeng', '2022-11-05 09:42:03', 'admin', 'MTg2NjU4MDI2MzY');
INSERT INTO `sp_sys_user` VALUES ('1184019107907227649', '超级管理员', 'admin', '7b18f9e91f009c2473b5ed3513549c08', '11', '75039960@qq.com', '18665802636', '0755-12345678', '0', '2020-12-01 00:00:00', '密码必须填写', '选填', '选填', '选填', '选填', '选填', '选填', '选填', '自动采集产品数据， 自动报工', '0', '2020-02-01 16:12:08', 'admin', '2023-11-27 10:19:53', 'admin', 'MTIzNDU2');
INSERT INTO `sp_sys_user` VALUES ('1588708706288308226', 'user1', 'user1', '706c12a092f7c4df778f200ece88d6e1', '', '', '', '', '0', NULL, '', '', '\"\"', '', '', '\"\"', '\"\"', '\"\"', '大家最好用不同的账户登录，方便做日志', '0', '2022-11-05 09:44:07', 'user', '2024-07-05 10:25:26', 'user1', NULL);
INSERT INTO `sp_sys_user` VALUES ('1810958621826248705', '曹操', 'user2', '9d782a4d1f83decb658f42ffc28dc9ad', '采购', 'fsdagsd@qq.com', '12345678912', '', '0', NULL, '', '', '\"\"', '', '', '\"\"', '\"\"', '\"\"', '复方丹参', '0', '2024-07-10 16:45:56', 'admin', '2024-07-21 13:19:09', 'admin', NULL);

-- ----------------------------
-- Table structure for sp_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sp_sys_user_role`;
CREATE TABLE `sp_sys_user_role`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户对应的角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_sys_user_role
-- ----------------------------
INSERT INTO `sp_sys_user_role` VALUES ('1267739082731270146', '1266201180838801409', '1336542182244384', '2020-06-02 16:45:25', 'admin', '2022-06-02 16:45:25', 'admin');
INSERT INTO `sp_sys_user_role` VALUES ('1280381244774002690', '1276512902757724162', '1232532514523213826', '2020-07-07 14:00:52', 'admin', '2022-07-07 14:00:52', 'admin');
INSERT INTO `sp_sys_user_role` VALUES ('1588708189130625026', '1184019107907227640', '1185025876737396738', '2022-11-05 09:42:04', 'admin', '2022-11-05 09:42:04', 'admin');
INSERT INTO `sp_sys_user_role` VALUES ('1728961810664775682', '1184019107907227649', '1185025876737396738', '2023-11-27 10:19:53', 'admin', '2023-11-27 10:19:53', 'admin');
INSERT INTO `sp_sys_user_role` VALUES ('1809050925619376130', '1588708706288308226', '1232532514523213826', '2024-07-05 10:25:26', 'user1', '2024-07-05 10:25:26', 'user1');
INSERT INTO `sp_sys_user_role` VALUES ('1814892850461171713', '1810958621826248705', '1730406078675673090', '2024-07-21 13:19:09', 'admin', '2024-07-21 13:19:09', 'admin');

-- ----------------------------
-- Table structure for sp_table_manager
-- ----------------------------
DROP TABLE IF EXISTS `sp_table_manager`;
CREATE TABLE `sp_table_manager`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '表名称',
  `table_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  `is_deleted` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '\"\"' COMMENT '授权(多个用逗号分隔，如：sys:menu:list,sys:menu:create)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index1`(`table_name` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '主数据通用管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_table_manager
-- ----------------------------
INSERT INTO `sp_table_manager` VALUES ('1283020801696837633', 'sp_bom', 'BOM配置', '2022-07-14 20:49:31', 'admin', '2022-10-17 23:35:53', 'admin', '0', '\"\"');
INSERT INTO `sp_table_manager` VALUES ('1582031940412919810', 'sp_config', '数据采集配置表', '2022-10-17 23:33:02', 'admin', '2022-11-04 19:26:53', 'admin', '0', '\"\"');
INSERT INTO `sp_table_manager` VALUES ('1582034379501359105', 'sp_global_id', '全局订单ID', '2022-10-17 23:42:44', 'admin', '2022-10-18 12:20:55', 'admin', '0', '\"\"');

-- ----------------------------
-- Table structure for sp_table_manager_item
-- ----------------------------
DROP TABLE IF EXISTS `sp_table_manager_item`;
CREATE TABLE `sp_table_manager_item`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `table_name_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '表名称id',
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字段',
  `field_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '字段描述',
  `must_fill` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否必填',
  `sort_num` int NOT NULL COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '主数据基础数据明细表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_table_manager_item
-- ----------------------------
INSERT INTO `sp_table_manager_item` VALUES ('1535145079321333761', '1283020801696837633', 'materiel_desc', '888', 'Y', 1, '2022-06-10 14:21:23', 'admin', '2022-06-10 14:21:23', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1535145079480717314', '1283020801696837633', 'materiel_code', '999', 'N', 2, '2022-06-10 14:21:23', 'admin', '2022-06-10 14:21:23', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1582225184245604354', '1582034379501359105', 'order_code', '订单编码', 'Y', 1, '2022-10-18 12:20:55', 'admin', '2022-10-18 12:20:55', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1582225184312713217', '1582034379501359105', 'line', '工艺线路', 'Y', 2, '2022-10-18 12:20:55', 'admin', '2022-10-18 12:20:55', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1588492976120864769', '1582031940412919810', 'line_id', '工艺线路', 'Y', 1, '2022-11-04 19:26:53', 'admin', '2022-11-04 19:26:53', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1588492976255082497', '1582031940412919810', 'server_ip', 'plc  ip', 'Y', 2, '2022-11-04 19:26:53', 'admin', '2022-11-04 19:26:53', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1588492976255082498', '1582031940412919810', 'server_port', 'plc  port', 'Y', 3, '2022-11-04 19:26:53', 'admin', '2022-11-04 19:26:53', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1588492976255082499', '1582031940412919810', 'local_port', 'pc   port', 'Y', 4, '2022-11-04 19:26:53', 'admin', '2022-11-04 19:26:53', 'admin');
INSERT INTO `sp_table_manager_item` VALUES ('1588492976255082500', '1582031940412919810', 'fronturl', '看板 url', 'Y', 5, '2022-11-04 19:26:53', 'admin', '2022-11-04 19:26:53', 'admin');

-- ----------------------------
-- Table structure for sp_test
-- ----------------------------
DROP TABLE IF EXISTS `sp_test`;
CREATE TABLE `sp_test`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_test
-- ----------------------------

-- ----------------------------
-- Table structure for sp_work_report
-- ----------------------------
DROP TABLE IF EXISTS `sp_work_report`;
CREATE TABLE `sp_work_report`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0' COMMENT 'id',
  `plan_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '日计划编码',
  `order_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编码',
  `plan_date` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '计划日期',
  `use_time` int NULL DEFAULT 30 COMMENT '生产该批产品耗时（分钟）',
  `maked_qty` int NULL DEFAULT 0 COMMENT '当前产量',
  `bad_qty` int(10) UNSIGNED ZEROFILL NULL DEFAULT 0000000000 COMMENT '次品数量',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `is_deleted` tinyint NOT NULL DEFAULT 0 COMMENT '逻辑删除：1 表示删除，0 表示未删除，2 表示禁用',
  `report_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '报工单编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_work_report
-- ----------------------------
INSERT INTO `sp_work_report` VALUES ('1822507265355141122', 'PLAN2024080902', 'DD2024080902', NULL, 100, 100, 0000000002, '2024-08-11 13:36:07', 'admin', '2024-08-12 00:09:05', 'admin', 0, 'Report2024080902');
INSERT INTO `sp_work_report` VALUES ('1822553916790788097', 'PLAN2024080901', 'DD2024080901', NULL, 100, 444, 0000000002, '2024-08-11 16:41:30', 'admin', '2024-08-12 00:54:18', 'admin', 0, 'R2024080901');
INSERT INTO `sp_work_report` VALUES ('1822573701716017154', 'PLAN2024080902', 'DD2024080902', NULL, 120, 100, 0000000001, '2024-08-11 18:00:07', 'admin', '2024-08-12 01:00:08', 'admin', 0, 'R2024080902');
INSERT INTO `sp_work_report` VALUES ('1822671745514065921', 'PLAN2024080901', 'DD2024080901', NULL, 60, 6, 0000000000, '2024-08-12 00:29:42', 'admin', '2024-08-12 00:59:25', 'admin', 0, 'WR2024080901-1');
INSERT INTO `sp_work_report` VALUES ('1822679979725434882', 'PLAN20240722-1', 'DD2024072201', NULL, 120, 300, 0000000001, '2024-08-12 01:02:26', 'admin', '2024-08-12 01:09:29', 'admin', 0, 'WR20240811-1');
INSERT INTO `sp_work_report` VALUES ('1822680702093635586', 'PLAN20240722-1', 'DD2024072201', NULL, 100, 100, 0000000001, '2024-08-12 01:05:18', 'admin', '2024-08-12 01:09:37', 'admin', 0, 'WR20240811-2');
INSERT INTO `sp_work_report` VALUES ('1822682863334600706', 'PLAN2024080902-1', 'DD2024080902', NULL, 90, 200, 0000000001, '2024-08-12 01:13:53', 'admin', '2024-08-12 01:42:11', 'admin', 0, 'WR20240811-3');

-- ----------------------------
-- Table structure for sp_work_shop
-- ----------------------------
DROP TABLE IF EXISTS `sp_work_shop`;
CREATE TABLE `sp_work_shop`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键id',
  `work_shop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车间',
  `work_shop_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车间描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '最后更新时间',
  `update_username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '工作车间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sp_work_shop
-- ----------------------------
INSERT INTO `sp_work_shop` VALUES ('1336875254022176', 'GCZ-01', '高磁阻变压器生产车间', '2022-06-29 11:29:57', 'admin', '2022-06-29 10:52:39', 'admin');
INSERT INTO `sp_work_shop` VALUES ('1336875591663648', 'CS-01', '测速传感器', '2022-06-29 11:32:38', 'admin', '2022-06-29 11:32:38', 'admin');

SET FOREIGN_KEY_CHECKS = 1;
